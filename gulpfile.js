const gulp            = require('gulp'),
      autoprefixer    = require('autoprefixer'),
      bs              = require('browser-sync').create(),
      cleancss        = require('gulp-clean-css'),
      del             = require('del'),
      filter          = require('gulp-filter'),
      imagemin        = require('gulp-imagemin'),
      pug             = require('gulp-pug'),
      sass            = require('gulp-sass'),
      sourcemaps      = require('gulp-sourcemaps');


// Styles Tasks

gulp.task('cleancss', function() {
  return gulp.src('./src/css/**/*.css')
    .pipe(sourcemaps.init())
    .pipe(cleancss({compatibility: 'ie10'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('sass', function() {
  return gulp.src('./src/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./src/css'))
    .pipe(bs.stream());
});

// Views Tasks

gulp.task('pug', function() {
  return gulp.src('./src/templates/**/*.pug')
    .pipe(pug({ pretty: true } ))
    .pipe(filter(function(file) {
      var exclude = new RegExp('core|mixins|includes', 'g');
      return !exclude.test(file.path);
    }))
    .pipe(gulp.dest('./src'))
    .pipe(bs.stream());
});

// Assets Tasks

gulp.task('fonts', function() {
  return gulp.src('./src/fonts/**/*')
    .pipe(gulp.dest('./dist/fonts/'));
});

gulp.task('imagemin', function() {
  return gulp.src('src/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/images'))
});

// Maintenance Tasks

gulp.task('clean', function() {
  return del.sync('dist');
});

gulp.task('copy', function() {
  return gulp.src('./src/*.html')
    .pipe(gulp.dest('./dist'));
});

// Main Tasks

gulp.task('default', ['serve']);

gulp.task('serve', function() {
  bs.init({
    server: {
      baseDir: './src'
    }
  });
  gulp.watch(['./src/scss/**/*.scss'], ['sass']);
  gulp.watch(['./src/templates/**/*.pug'], ['pug']);
});

gulp.task('build', ['clean', 'cleancss', 'copy', 'fonts', 'imagemin', 'pug'], function() {
  console.log('building...');
});
